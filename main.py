import re
import os
import numpy as np
from googletrans import Translator

# List all file inside the given directory.
def get_list_of_files(dirName):
  all_files = list()
  
  for entry in os.listdir(dirName):
    fullPath = os.path.join(dirName, entry)
    
    if os.path.isdir(fullPath):
      all_files = all_files + get_list_of_files(fullPath)
    else:
      all_files.append(fullPath)
                
  return all_files

file_contents = list()

# Read file contents.
for i in get_list_of_files('./resources'):
  file_item = open(i, 'r')
  file_contents.append(file_item.read())
  file_item.close()

# Dictionary function pattern __('dict.key123').
pattern = "\_\_\(\'dict\.\w*\'\)"

matches = np.array([])

# Stripe key name.
key_lambda = lambda str: str.replace("__('dict.", '').replace("')", '')
get_key = np.vectorize(key_lambda)

# Find all matches.
for content in file_contents:
  matches = np.append(matches, re.findall(pattern, content))

# Take only unique matches.
unique_matches = np.unique(matches)
transformed_keys = get_key(unique_matches)

en_arr = dict()
pt_arr = dict()

# Turn key into value by replacing all underscores.
def getValue(key):
  return key.capitalize().replace('_', ' ')

# Translate with Google translate.
def translate(str):
  translator = Translator()

  return translator.translate(str, src="en", dest="pt")

# Populate en and pt lists.
for i in range(len(transformed_keys)):
  enVal = str(getValue(transformed_keys[i]))
  ptVal = translate(enVal).text.capitalize()

  en_arr.update({transformed_keys[i] : enVal})
  pt_arr.update({transformed_keys[i] : ptVal})

print('------------------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print('ENGLISH-----------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print(en_arr)

print('------------------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print('PORTUGUESE--------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print('------------------------------------------------------')
print(pt_arr)
